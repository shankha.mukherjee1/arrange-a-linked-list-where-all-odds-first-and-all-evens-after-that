package com.mevero.pursue.dao.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Demo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in); // System.in is a standard input stream.
		System.out.print("Enter total no you want to arrange .... ");
		int totalNum = sc.nextInt();
		List<Integer> list = new LinkedList<>(); // You can create ArrayList object
		System.out.print("Enter all numbers .... ");
		for (int i = 1; i <= totalNum; i++) {

			list.add(sc.nextInt());
		}
		List<Integer> oddList = new LinkedList<>();
		List<Integer> evenList = new LinkedList<>();
		for (int i = 0; i < totalNum; i++) {

			if (list.get(i) % 2 == 0) {
				evenList.add(list.get(i));

			} else
				oddList.add(list.get(i));
		}
		list.clear();
		// Collections.sort(oddList); If you want to sort please uncomment it
		// Collections.sort(evenList); If you want to sort please uncomment it
		list.addAll(oddList);
		list.addAll(evenList);

		System.out.println(list);
	}

}
